# Tournaments

Software for planning and keeping score of tournaments in various formats.

## Overview

Tournaments is a software meant to plot and keep track of competitions in various sports. 

### Problem

Keeping track of tournaments can get messy and, at times, can even ruin the whole competition. That's where Tournaments comes in.

### Goals

* Support for various sports (football, basketball, volleybal...)
* Support for different tournament formats (shown later in the document)
* Accessibility and easy-to-use interface
* Nice graphs and whatnot

All of these goals should serve as metrics of success - the more sports and formats are supported while not making the whole software a mess, the better.

#### V1.0

* Swiss-system
* Web based
* Basic UI

## Current Solutions

### Web-based or mobile apps

They can be hard to use, ugly or just old and unintuitive. Sometimes all at the same time.

### Excel Spredsheets

They are infuriating. They are old school. They are not so pretty.

### Handwritten tables

Same as Excel. But worse. Much worse.

## Proposed Solution

Nice and clean web-app made with React and other frameworks. Choosing a format, sport, setting up competitors and going through the whole tournament should be easy and fun.